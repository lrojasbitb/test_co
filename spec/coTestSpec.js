const expect = require('chai').expect;

const coTest = require('../src/coTest');
const CarInsurance = coTest.CarInsurance;

const Product = require('../src/models/product').Product;
const MediumCoverage = require('../src/models/mediumCoverage').MediumCoverage;
const FullCoverage = require('../src/models/fullCoverage').FullCoverage;
const LowCoverage = require('../src/models/lowCoverage').LowCoverage;
const MegaCoverage = require('../src/models/megaCoverage').MegaCoverage;
const SpecialFullCoverage = require('../src/models/specialFullCoverage').SpecialFullCoverage;
const SuperSale = require('../src/models/superSale').SuperSale;

describe("Co Test", function() {

  it("should foo", function() {
    const coTest = new CarInsurance([ new Product("foo", 0, 0) ]);
    const products = coTest.updatePrice();
    expect(products[0].name).equal("foo");
  });

  it("MediumCoverage price decrement", function() {
    const coTest = new CarInsurance([ new MediumCoverage("MediumCoverage Product", 3, 6) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(2);
    expect(products[0].price).equal(5);
  });

  it("MediumCoverage <0 price decrement", function() {
    const coTest = new CarInsurance([ new MediumCoverage("MediumCoverage Product", -4, 10) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(-5);
    expect(products[0].price).equal(8);
  });

  it("FullCoverage price increment", function() {
    const coTest = new CarInsurance([ new FullCoverage("FullCoverage Product", 2, 0) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(1);
    expect(products[0].price).equal(1);
  });

  it("FullCoverage max price", function() {
    const coTest = new CarInsurance([ new FullCoverage("FullCoverage Product", 5, 50) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(4);
    expect(products[0].price).equal(50);
  });

  it("LowCoverage price decrement", function() {
    const coTest = new CarInsurance([ new LowCoverage("LowCoverage Product", 3, 6) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(2);
    expect(products[0].price).equal(5);
  });

  it("LowCoverage <0 price decrement", function() {
    const coTest = new CarInsurance([ new LowCoverage("LowCoverage Product", -4, 10) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(-5);
    expect(products[0].price).equal(8);
  });

  it("MegaCoverage price and sellIn does not change", function() {
    const coTest = new CarInsurance([ new MegaCoverage("MegaCoverage Product", 0, 80) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(0);
    expect(products[0].price).equal(80);
  });

  it("SpecialFullCoverage normal price increment", function() {
    const coTest = new CarInsurance([ new SpecialFullCoverage("SpecialFullCoverage Product", 15, 20) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(14);
    expect(products[0].price).equal(21);
  });

  it("SpecialFullCoverage 5> <10 price increment", function() {
    const coTest = new CarInsurance([ new SpecialFullCoverage("SpecialFullCoverage Product", 9, 30) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(8);
    expect(products[0].price).equal(32);
  });

  it("SpecialFullCoverage <5 price increment", function() {
    const coTest = new CarInsurance([ new SpecialFullCoverage("SpecialFullCoverage Product", 4, 20) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(3);
    expect(products[0].price).equal(23);
  });

  it("SuperSale normal price decrement", function() {
    const coTest = new CarInsurance([ new SuperSale("SuperSale Product", 3, 6) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(2);
    expect(products[0].price).equal(4);
  });

  it("SuperSale sellin <0 price decrement", function() {
    const coTest = new CarInsurance([ new SuperSale("SuperSale Product", -1, 8) ]);
    const products = coTest.updatePrice();
    expect(products[0].sellIn).equal(-2);
    expect(products[0].price).equal(4);
  });
});
