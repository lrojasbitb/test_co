const coTest = require('./src/coTest');
const CarInsurance = coTest.CarInsurance;
const Product = require('./src/models/product').Product;
const MediumCoverage = require('./src/models/mediumCoverage').MediumCoverage;
const FullCoverage = require('./src/models/fullCoverage').FullCoverage;
const LowCoverage = require('./src/models/lowCoverage').LowCoverage;
const MegaCoverage = require('./src/models/megaCoverage').MegaCoverage;
const SpecialFullCoverage = require('./src/models/specialFullCoverage').SpecialFullCoverage;
const SuperSale = require('./src/models/superSale').SuperSale;

//Product objects are kept for comparison, can be removed if needed
const productsAtDayZero = [
  // new Product('Medium Coverage', 10, 20),
  // new Product('Full Coverage', 2, 0),
  // new Product('Low Coverage', 5, 7),
  // new Product('Mega Coverage', 0, 80),
  // new Product('Mega Coverage', -1, 80),
  // new Product('Special Full Coverage', 15, 20),
  // new Product('Special Full Coverage', 10, 49),
  // new Product('Special Full Coverage', 5, 49),
  // new Product('Super Sale', 3, 6),
  //Product objects
  new MediumCoverage('Medium Coverage Product', 10, 20),
  new FullCoverage('Full Coverage Product', 2, 0),
  new LowCoverage('Low Coverage Product', 5, 7),
  new MegaCoverage('Mega Coverage Product', 0, 80),
  new MegaCoverage('Mega Coverage Product', -1, 80),
  new SpecialFullCoverage('Special Full Coverage Product', 15, 20),
  new SpecialFullCoverage('Special Full Coverage Product', 10, 49),
  new SpecialFullCoverage('Special Full Coverage Product', 5, 49),
  new SuperSale('Super Sale Product', 3, 6),

];

const carInsurance = new CarInsurance(productsAtDayZero);
const productPrinter = function (product) {
  console.log(`${product.name}, ${product.sellIn}, ${product.price}`);
};

for (let i = 1; i <= 30; i += 1) {
  console.log(`Day ${i}`);
  console.log('name, sellIn, price');
  carInsurance.updatePrice().forEach(productPrinter);
  console.log('');
}