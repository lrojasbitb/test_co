const Product = require('./product.js').Product;

class MediumCoverage extends Product {
  constructor(name, sellIn, price) {
    super(name, sellIn, price);
  }

  updateProductPrice() {
    if(this.price <= 50){
      if(this.price > 0){
        if(this.sellIn > 0){
          this.price--;
        }
        else{
          this.price -=2;
        }
      }
    }
    this.sellIn--;
  }
}
module.exports = {
  MediumCoverage
}