const Product = require('./product.js').Product;

class SpecialFullCoverage extends Product {
  constructor(name, sellIn, price) {
    super(name, sellIn, price);
  }

  updateProductPrice() {
    if(this.sellIn==0){ this.price = 0 }
    if(this.price < 50){
      if(this.sellIn > 0){
        let new_price = 0;
        if(this.sellIn > 10){
          new_price = this.price+1;
        } else if (this.sellIn > 5){
            new_price = this.price+2;
        } else{
          new_price = this.price+3;
        }
        new_price < 50 ? this.price = new_price : this.price = 50;
      }
    }
    this.sellIn--;
  }
}
module.exports = {
  SpecialFullCoverage
}