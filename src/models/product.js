class Product {
  constructor(name, sellIn, price) {
    this.name = name;
    this.sellIn = sellIn;
    this.price = price;
  }
  // logic from original coTest.js updatePrice
  updateProductPrice() {
    if (this.name != 'Full Coverage' && this.name != 'Special Full Coverage') {
      if (this.price > 0) {
        if (this.name != 'Mega Coverage') {
          this.price = this.price - 1;
        }
      }
    } else {
      if (this.price < 50) {
        this.price = this.price + 1;
        if (this.name == 'Special Full Coverage') {
          if (this.sellIn < 11) {
            if (this.price < 50) {
              this.price = this.price + 1;
            }
          }
          if (this.sellIn < 6) {
            if (this.price < 50) {
              this.price = this.price + 1;
            }
          }
        }
      }
    }
    if (this.name != 'Mega Coverage') {
      this.sellIn = this.sellIn - 1;
    }
    if (this.sellIn < 0) {
      if (this.name != 'Full Coverage') {
        if (this.name != 'Special Full Coverage') {
          if (this.price > 0) {
            if (this.name != 'Mega Coverage') {
              this.price = this.price - 1;
            }
          }
        } else {
          this.price = this.price - this.price;
        }
      } else {
        if (this.price < 50) {
          this.price = this.price + 1;
        }
      }
    }
  }
}
module.exports = {
  Product
}