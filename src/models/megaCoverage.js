const Product = require('./product.js').Product;

class MegaCoverage extends Product {
  constructor(name, sellIn, price) {
    super(name, sellIn, price);
  }

  updateProductPrice() {
    //Does not change sellIn or price value
  }
}
module.exports = {
  MegaCoverage
}